package com.iran.encrypt;

import android.annotation.SuppressLint;
import android.app.Application;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class App extends Application {

    public static Context context;
    public static AppCompatActivity activity;
    public static Handler HANDLER = new Handler();
    public static SharedPreferences.Editor editor;
    public static SharedPreferences sharedPreferences;
    public static Intent serviceIntent;
    public static NotificationManager notificationManager;

    @SuppressLint("HardwareIds")
    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = sharedPreferences.edit();
        editor.apply();
        notificationManager = (NotificationManager) App.context.getSystemService(Context.NOTIFICATION_SERVICE);

        App.secKey = App.sharedPreferences.getString("SEC_KEY", "XF8wfQBEJ1");

    }

    public static long time() {
        return System.currentTimeMillis() / 1000;
    }

    public static String getExeptionString(Exception e) {
        return "ERROR: " +
                e.getMessage() +
                " at " + e.getStackTrace()[0].getLineNumber() +
                " in " + e.getStackTrace()[0].getClassName() +
                " (" + e.getStackTrace()[0].getMethodName() + ")";
    }

    public static String md5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuilder sb = new StringBuilder();
            for (byte anArray : array) {
                sb.append(Integer.toHexString((anArray & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();

        } catch (java.security.NoSuchAlgorithmException ignored) {
        }
        return null;
    }

    public static String secKey;

    @SuppressLint("GetInstance")
    public static String mEncrypt(String value) {
        try {
            DESKeySpec desKeySpec;
            desKeySpec = new DESKeySpec(secKey.getBytes("UTF8"));
            SecretKeyFactory secretKeyFactory = null;
            secretKeyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secretKey = null;
            if (secretKeyFactory != null) {
                secretKey = secretKeyFactory.generateSecret(desKeySpec);
            }

            byte[] clearText = value.getBytes("UTF8");
            Cipher cipher;
            cipher = Cipher.getInstance("DES");
            if (cipher != null) {
                cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            }

            String encrypedValue = null;
            if (cipher != null) {
                encrypedValue = Base64.encodeToString(cipher.doFinal(clearText), Base64.DEFAULT);
            }
            return encrypedValue;

        } catch (Exception e) {
            Log.i("TAG", App.getExeptionString(e));
        }
        return value;
    }

    public static String mDecrypt(String value) {
        try {
            DESKeySpec desKeySpec = new DESKeySpec(secKey.getBytes("UTF8"));
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secretKey = secretKeyFactory.generateSecret(desKeySpec);

            byte[] encrypedPwdBytes = Base64.decode(value, Base64.DEFAULT);
            @SuppressLint("GetInstance") Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            byte[] decrypedValueBytes = (cipher.doFinal(encrypedPwdBytes));

            return new String(decrypedValueBytes);

        } catch (Exception e) {
            Log.i("TAG", App.getExeptionString(e));
        }
        return value;
    }

}

