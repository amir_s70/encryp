package com.iran.encrypt;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.getkeepsafe.taptargetview.TapTarget;
import com.getkeepsafe.taptargetview.TapTargetSequence;

public class MainActivity extends AppCompatActivity {

    TextView sec;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setTitle("");


        App.serviceIntent = new Intent(this, ManagerService.class);
        startService(App.serviceIntent);

        final EditText editText = findViewById(R.id.edit);
        final TextView textView = findViewById(R.id.res);
        sec = findViewById(R.id.sec);

        CardView box = findViewById(R.id.box);

        box.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText(textView.getText().toString(), textView.getText().toString());
                if (clipboard != null) {
                    clipboard.setPrimaryClip(clip);
                    Toast.makeText(getApplicationContext(), "Copied to clipboard", Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.deleteAll).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editText.setText("");
            }
        });

        sec.setText(App.secKey);

        sec.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                App
                        .editor
                        .putString("SEC_KEY", s.toString())
                        .apply();
                App.secKey = s.toString();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        findViewById(R.id.btnSafe).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sec.getText().toString().length() < 8) {
                    Toast.makeText(getApplicationContext(), "کلید امنیتی کافی نمی‌باشد", Toast.LENGTH_LONG).show();
                    return;
                }
                String secu = sec.getText().toString();
                String s = editText.getText().toString();
                String s1 = App.mEncrypt(s);

                textView.setText(s1.equals(s) ? "ایمن سازی با مشکل روبرو شد" : s1);

                App
                        .editor
                        .putString("SEC_KEY", secu)
                        .apply();
                App.secKey = secu;

            }
        });

        findViewById(R.id.btnDec).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sec.getText().toString().length() < 8) {
                    Toast.makeText(getApplicationContext(), "کلید امنیتی کافی نمی‌باشد", Toast.LENGTH_LONG).show();
                    return;
                }
                String s = editText.getText().toString();
                String secu = sec.getText().toString();
                String s1 = App.mDecrypt(s);

                textView.setText(s1.equals(s) ? "رمزیابی با مشکل روبرو شد" : s1);

                App
                        .editor
                        .putString("SEC_KEY", secu)
                        .apply();
                App.secKey = secu;

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        App.HANDLER.post(new Runnable() {
            @Override
            public void run() {
                showHelp();
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.ref:
                String s = App.md5(App.time() + "");
                for (int i = 0; i < 1000; i++) {
                    s = App.md5(App.time() + "");
                }
                s = s != null ? s.substring(0, 8) : null;
                App
                        .editor
                        .putString("SEC_KEY", s)
                        .apply();
                App.secKey = s;
                sec.setText(s);
                break;
            case R.id.help:
                App.editor.remove("noShowHelp").apply();
                showHelp();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showHelp() {
        if (!App.sharedPreferences.getBoolean("noShowHelp", false)) {
            App.editor.putBoolean("noShowHelp", true).apply();
            new TapTargetSequence(this)
                    .targets(
                            TapTarget.forView(sec, "کلید امنیتی", "این کلید حکم پسورد متن شما را دارد\nاین کلید را فقط در اختیار کسانی قرار دهید که حق خواندن متن های رمزنگاری شده را داشته باشند")
                                    .outerCircleColor(R.color.colorAccent)
                                    .targetCircleColor(android.R.color.white)
                                    .titleTextColor(android.R.color.white)
                                    .transparentTarget(true),
                            TapTarget.forView(findViewById(R.id.ref), "ساخت کلید امنیتی", "اگر میخواهید یک کلید امنیتی جدید تولید کنید، می‌توانید با لمس این آیکن، این کار را انجام دهید.\nپس از تغییر کلید امنیتی، آن را در اختیار بقیه افراد قابل اعتماد هم قرار دهید")
                                    .outerCircleColor(R.color.colorBlueDarker)
                                    .targetCircleColor(android.R.color.white)
                                    .titleTextColor(android.R.color.white)
                                    .transparentTarget(true)
                    )
                    .start();
        }
    }

}
