package com.iran.encrypt;

import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import Classes.NotificationCreate;

public class ManagerService extends Service {
    private static final String TAG = "TAG";

    public static ManagerService service;
    private ClipboardManager mClipboardManager;

    @Override
    public void onCreate() {
        super.onCreate();

        service = this;

        mClipboardManager =
                (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        if (mClipboardManager != null) {
            mClipboardManager.addPrimaryClipChangedListener(
                    mOnPrimaryClipChangedListener);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mClipboardManager != null) {
            mClipboardManager.removePrimaryClipChangedListener(
                    mOnPrimaryClipChangedListener);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private ClipboardManager.OnPrimaryClipChangedListener mOnPrimaryClipChangedListener = new ClipboardManager.OnPrimaryClipChangedListener() {
        @Override
        public void onPrimaryClipChanged() {
            ClipData clip = mClipboardManager.getPrimaryClip();
            String s1 = clip.getItemAt(0).getText().toString();
            Log.i(TAG, s1);

            String s = App.mDecrypt(s1);
            if (s.equals(s1)) return;

            new NotificationCreate()
                    .setTitle("دکد شد")
                    .setBody(s)
                    .setBigBody(s)
                    .notif();

        }
    };

}