package Classes;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.iran.encrypt.App;
import com.iran.encrypt.R;

public class NotificationCreate {

    private NotificationCompat.Builder builder;
    private NotificationCompat.BigTextStyle bigText;
    private int id = 0;

    public NotificationCreate(int id) {

        this.id = id;

        // Configure the channel
        builder = new NotificationCompat.Builder(App.context);

        builder.setSmallIcon(R.mipmap.rounded);
        builder.setLargeIcon(BitmapFactory.decodeResource(App.context.getResources(), R.mipmap.logo));

        builder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
        builder.setVibrate(new long[]{0, 250, 250, 250});
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            builder.setPriority(Notification.PRIORITY_HIGH);
        }

        bigText = new NotificationCompat.BigTextStyle();

    }

    public NotificationCreate() {
        this(0);
    }

    public NotificationCreate setTitle(String title) {
        bigText.setBigContentTitle(title);
        builder.setContentTitle(title);
        return this;
    }

    public NotificationCreate setIntent(Intent intent) {
        int requestID = (int) System.currentTimeMillis();
        int flags = PendingIntent.FLAG_CANCEL_CURRENT;
        PendingIntent pIntent = PendingIntent.getActivity(App.context, requestID, intent, 0);
        builder.setContentIntent(pIntent);
        return this;
    }

    public NotificationCreate addAction(int drawable, String title, Intent intent) {
        int requestID = (int) System.currentTimeMillis();
        int flags = PendingIntent.FLAG_CANCEL_CURRENT;
        PendingIntent pIntent = PendingIntent.getActivity(App.context, requestID, intent, 0);
        builder.addAction(drawable, title, pIntent);
        return this;
    }

    public NotificationCreate addAction(String title, Intent intent) {
        return addAction(0, title, intent);
    }

    public NotificationCreate setAutoCancle(boolean b) {
        builder.setAutoCancel(b);
        return this;
    }

    public NotificationCreate setDefaults(int i) {
        builder.setDefaults(i);
        return this;
    }

    public NotificationCreate setSound(Uri uri) {
        builder.setSound(uri);
        return this;
    }

    public NotificationCreate setBody(String body) {
        bigText.setSummaryText(body);
        builder.setContentText(body);
        return this;
    }

    public NotificationCreate setBigBody(String body) {
        bigText.bigText(body);
        return this;
    }

    public NotificationCreate setSmallIcon(int drawable) {
        builder.setSmallIcon(drawable);
        return this;
    }

    public NotificationCreate setLargeIcon(int drawable) {
        builder.setLargeIcon(BitmapFactory.decodeResource(App.context.getResources(), drawable));
        return this;
    }

    public NotificationCreate setContentInfo(CharSequence info) {
        builder.setContentInfo(info);
        return this;
    }

    public NotificationCreate large(String longText) {
        builder.setStyle(new NotificationCompat.BigTextStyle().bigText(longText));
        return this;
    }

    public void notif() {

        builder.setStyle(bigText);


        if (App.notificationManager != null) {
            App.notificationManager.notify(id, builder.build());
        }

    }

}
